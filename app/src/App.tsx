import React, { useState } from 'react';
import './App.css';

function App()
{
	const [coords, setCoords] = useState<GeolocationCoordinates>();
	const [isOnline, setIsOnline] = useState(navigator.onLine);

	if (navigator.geolocation)
	{
		navigator.geolocation.watchPosition(
		(position) =>
		{
			setCoords(position.coords);
		},
		(error) =>
		{
			console.error(error);
		},
		{
			enableHighAccuracy: true
		});
	}

	window.addEventListener('online', () =>
	{
		setIsOnline(true);
	});

	window.addEventListener('offline', () =>
	{
		setIsOnline(false);
	});

	return (
		<div className="App">
			<header className="App-header">
				<p>
					{ coords && `Latitute ${coords.latitude}` }
				</p>
				<p>
					{ coords && `Longitude: ${coords.longitude}` }
				</p>
			</header>
			{!isOnline &&
				<div className="offline">
					<div className="offline-msg">You are working offline</div>
				</div>
			}
		</div>
	);
}

export default App;
