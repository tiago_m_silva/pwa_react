# From PWA to TWA

## High Level Steps

1. Register the service worker in the index.tsx by changing to .unregister() to .register()
2. Add the `"purpose": "any maskable"` property to the logo512 icon in the manifest.json (check: https://web.dev/maskable-icon/)
3. Create the App.tsx component
4. Run: `docker-compose up -d --build --force-recreate`
5. Go to http://localhost:4551/ and get the HTTPS ngrok link
6. Paste the ngrok link in https://www.pwabuilder.com/
7. Download the .zip
8. Copy the assetlinks.json to app/public/.well-known/assetlinks.json
9. Run: `docker cp .\app\public\.well-known\assetlinks.json pwa_frontend:/app/build/.well-known/assetlinks.json`
10. Drag and drop the .apk to the emulator

## Limitations
- Docker development environment not working. Production is working
- Service Worker only works in production mode
- Lighthouse test should be made without ngrok because of the free limit of the service.
- After building the TWA wait a minute before clicking the download because of the same reason above
